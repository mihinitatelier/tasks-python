# Написать функцию season, принимающую 1 аргумент — номер месяца (от 1 до 12), и возвращающую время года,
# которому этот месяц принадлежит (зима, весна, лето или осень).


def season(number):

    if number in (1, 2, 12):
        return "winter"
    elif number in (3, 4, 5):
        return "spring"
    elif number in (6, 7, 8):
        return "summer"
    elif number in (9, 10, 11):
        return "autumn"

season(9)