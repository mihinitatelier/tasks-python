#Задача 6. Напишите программу, которая будет выводить в консоль ваш IP-адрес выхода в Интернет (реальный).
#Используйте API облачного сервиса https://www.ipify.org/. Вывод должен иметь формат "Ваш IP адрес: xxx.xxx.xxx.xxx"

from requests import get

def myip():

    print('Ваш IP адрес: {}'.format(get('https://api.ipify.org').text))

myip()