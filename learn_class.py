from datetime import datetime


class Person(object):
    given_name = None
    family_name = None

    def __init__(self):
        self.person_list = []
        return

    def set_person(self, person_id):
        """Установить значения имени и фамилии по умолчанию, по id сотрудника"""
        if person_id < self.get_free_person_id():
            self.family_name = self.get_family_name(person_id)
            self.given_name = self.get_given_name(person_id)
            return
        else:
            print(f'Ошибка! Персона с Id={person_id} не найдена, уточните запрос.')

    def get_person_id(self,family_name, given_name):
        """Получить id сотрудника по имени и фамилии"""
        person_id = 0
        for person in self.person_list:
            get_family_name = person['family_name']
            get_given_name = person['given_name']
            if family_name == get_family_name and given_name == get_given_name:
                #print(f"Сотруднику {family_name} {given_name} принадлежит идентификатор с номером {person_id}")
                print(person_id)

    def get_free_person_id(self):
        """получить свободный id(index) из списка сотрудников"""
        free_person_id = len(self.person_list)
        return free_person_id

    def add_person(self, family_name=None, given_name=None):
        """Добавить сотрудника"""
        id = self.get_free_person_id()
        self.person_list.append({'family_name': f'{family_name}', 'given_name': f'{given_name}'})
        print(f'Добавлен сотрудник {family_name} {given_name} c id={id}')

    def get_family_name(self, person_id):
        """Получить фамилию сотрудника по id"""
        family_name = f'{self.person_list[person_id]["family_name"]}'
        return family_name

    def get_given_name(self, person_id):
        """Получить имя сотрудника по id"""
        given_name = f'{self.person_list[person_id]["given_name"]}'
        return given_name

    def get_fullname(self, person_id):
        """Получить полное имя сотрудника по id"""
        fullname = f'{self.get_family_name(person_id)} {self.get_given_name(person_id)}'
        return fullname


class GateCounter(Person):
    datetime_format = "%d.%m.%Y %H:%M:%S"
    __DIRECTION_IN = 1
    __DIRECTION_OUT = 2
    __DIRECTIONS = {__DIRECTION_IN: "Enter", __DIRECTION_OUT: "Exit"}

    def __init__(self):
        super().__init__()
        self.move_counter = []
        return

    def move(self,person_id, direction):
        """Метод добавления направления движения"""
        self.set_person(person_id)
        now_time = datetime.now()
        log_entry = {'date': now_time,
                     'fullname': f'{self.family_name} {self.given_name}',
                     'direction': self.__DIRECTIONS[direction]}
        self.move_counter.append(log_entry)
        return

    def get_datetime_format(self, date):
        """Метод форматирования времени и даты"""
        hrf_date = date.strftime(self.datetime_format)
        return hrf_date

    def print_counter_list(self):
        """Метод печати списка действий"""
        for entry in self.move_counter:
            entry_date = self.get_datetime_format(entry["date"])
            print(f'{entry_date} {entry["fullname"]} {entry["direction"]}')

    def set_datetime_format(self, format):
        """Метод установки формата даты и времени"""
        self.datetime_format = format
        return

gc = GateCounter()

gc.add_person(family_name='Ivanov', given_name='Ivan')
gc.add_person(family_name='Egorova', given_name='Evgeniya')
gc.add_person(family_name='Smirnov', given_name='Vladimir')
gc.add_person(family_name='Drenichev', given_name='Denis')

gc.move(person_id=1, direction=1)

gc.move(person_id=100, direction=1)

gc.move(person_id=0, direction=1)
gc.move(person_id=2, direction=1)
gc.move(person_id=3, direction=1)
gc.move(person_id=2, direction=2)

gc.print_counter_list()
gc.set_datetime_format('==%d-%m-%Y==')
gc.print_counter_list()

